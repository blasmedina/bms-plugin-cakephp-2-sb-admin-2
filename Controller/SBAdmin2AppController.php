<?php

App::uses('AppController', 'Controller');

class SBAdmin2AppController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
        $navItems = $this->__getNavItems();
        $this->set('navItems', $navItems);
    }

    private function __getNavItems()
    {
        $controllerClasses = App::objects('controller');
        $excluir = array(
            'App',
            'Pages',
        );
        $navItems = array();
        foreach ($controllerClasses as $controller) {
            $name = str_replace("Controller", "", $controller);
            if (!in_array($name, $excluir)) {
                $navItem = array(
                    'href' => array(
                        'controller' => Inflector::underscore($name),
                        'action' => 'index',
                    ),
                    'icon'  => 'fa-cog',
                    'text'  => $name,
                    // 'items' => array(
                    //     array(
                    //         'href' => '/',
                    //         'icon' => 'fa-tachometer-alt',
                    //         'text' => 'Dashboard',
                    //     )
                    // ),
                );
                array_push($navItems, $navItem);
            }
        }
        return $navItems;
    }
}
