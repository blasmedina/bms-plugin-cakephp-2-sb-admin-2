<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta content="IE=edge" http-equiv="X-UA-Compatible" />
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <title><?php echo $this->fetch('title'); ?></title>
        <!-- Custom fonts for this template-->
        <?php echo $this->Html->css('SBAdmin2.vendor/fontawesome-free/css/all.min.css'); ?>
        <?php echo $this->Html->css('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); ?>
        <!-- Custom styles for this template-->
        <?php echo $this->Html->css('SBAdmin2.css/sb-admin-2.min.css'); ?>
        <?php echo $this->fetch('css'); ?>
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php echo $this->element('SBAdmin2.sidebar', array('navItems' => $navItems)); ?>
            <!-- Content Wrapper -->
            <div class="d-flex flex-column" id="content-wrapper">
                <!-- Main Content -->
                <div id="content">
                    <?php echo $this->element('SBAdmin2.topbar'); ?>
                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        <?php echo $this->fetch('content'); ?>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->
                <?php echo $this->element('SBAdmin2.footer'); ?>
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
        <?php echo $this->element('SBAdmin2.modal'); ?>
        <?php echo $this->fetch('modal'); ?>
        <!-- Bootstrap core JavaScript-->
        <?php echo $this->Html->script('SBAdmin2.vendor/jquery/jquery.min.js'); ?>
        <?php echo $this->Html->script('SBAdmin2.vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>
        <!-- Core plugin JavaScript-->
        <?php echo $this->Html->script('SBAdmin2.vendor/jquery-easing/jquery.easing.min.js'); ?>
        <!-- Custom scripts for all pages-->
        <?php echo $this->Html->script('SBAdmin2.js/sb-admin-2.min.js'); ?>
        <?php echo $this->fetch('script'); ?>
    </body>
</html>