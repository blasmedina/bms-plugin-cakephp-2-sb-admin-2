<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">
    <?php echo $this->element('SBAdmin2.topbar-nav-search-dropdown'); ?>
    <?php // echo $this->element('SBAdmin2.topbar-nav-alerts'); ?>
    <?php // echo $this->element('SBAdmin2.topbar-nav-messages'); ?>
    <div class="topbar-divider d-none d-sm-block"></div>
    <?php echo $this->element('SBAdmin2.topbar-nav-user-information'); ?>
</ul>