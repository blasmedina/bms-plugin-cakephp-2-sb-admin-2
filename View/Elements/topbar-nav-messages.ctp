<!-- Nav Item - Messages -->
<li class="nav-item dropdown no-arrow mx-1">
    <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="messagesDropdown" role="button">
        <i class="fas fa-envelope fa-fw">
        </i>
        <!-- Counter - Messages -->
        <span class="badge badge-danger badge-counter">
            7
        </span>
    </a>
    <!-- Dropdown - Messages -->
    <div aria-labelledby="messagesDropdown" class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in">
        <h6 class="dropdown-header">
            Message Center
        </h6>
        <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
                <img alt="" class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60">
                    <div class="status-indicator bg-success">
                    </div>
                </img>
            </div>
            <div class="font-weight-bold">
                <div class="text-truncate">
                    Hi there! I am wondering if you can help me with a problem I've been having.
                </div>
                <div class="small text-gray-500">
                    Emily Fowler · 58m
                </div>
            </div>
        </a>
        <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
                <img alt="" class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60">
                    <div class="status-indicator">
                    </div>
                </img>
            </div>
            <div>
                <div class="text-truncate">
                    I have the photos that you ordered last month, how would you like them sent to you?
                </div>
                <div class="small text-gray-500">
                    Jae Chun · 1d
                </div>
            </div>
        </a>
        <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
                <img alt="" class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60">
                    <div class="status-indicator bg-warning">
                    </div>
                </img>
            </div>
            <div>
                <div class="text-truncate">
                    Last month's report looks great, I am very happy with the progress so far, keep up the good work!
                </div>
                <div class="small text-gray-500">
                    Morgan Alvarez · 2d
                </div>
            </div>
        </a>
        <a class="dropdown-item d-flex align-items-center" href="#">
            <div class="dropdown-list-image mr-3">
                <img alt="" class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60">
                    <div class="status-indicator bg-success">
                    </div>
                </img>
            </div>
            <div>
                <div class="text-truncate">
                    Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...
                </div>
                <div class="small text-gray-500">
                    Chicken the Dog · 2w
                </div>
            </div>
        </a>
        <a class="dropdown-item text-center small text-gray-500" href="#">
            Read More Messages
        </a>
    </div>
</li>