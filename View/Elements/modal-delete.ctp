<div class="modal" id="modalDelete" tabindex="-1" role="dialog">
    <input type="hidden" name="">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="confim"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
                <?php echo $this->Html->link(__('Delete'), array('action' => 'delete'), array('class' => 'btn btn-danger delete')); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->Html->script('SBAdmin2.js/fix-modal-delete', array('inline' => false)); ?>