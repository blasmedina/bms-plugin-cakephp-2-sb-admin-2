<!-- Topbar Search -->
<form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
    <div class="input-group">
        <input aria-describedby="basic-addon2" aria-label="Search" class="form-control bg-light border-0 small" placeholder="Search for..." type="text">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm">
                    </i>
                </button>
            </div>
        </input>
    </div>
</form>