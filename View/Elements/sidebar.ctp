<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <?php echo $this->element('SBAdmin2.sidebar-brand'); ?>
    <!-- Divider -->
    <hr class="sidebar-divider my-0" />
    <?php foreach ($navItems as $item) {?>
        <?php $hash = 'collapse'. md5(print_r($item, true)); ?>
        <li class="nav-item">
            <?php if (!isset($item['items'])) { ?>
                <?php echo $this->Html->link(
                    $this->Html->tag('i', "", array('class' => 'fas fa-fw ' . $item['icon'])).$this->Html->tag('span', $item['text']),
                    $item['href'],
                    array(
                        'escape' => false,
                        'class' => 'nav-link'
                    )
                ); ?>
            <?php } else { ?>
                <?php echo $this->Html->link(
                    $this->Html->tag('i', "", array('class' => 'fas fa-fw ' . $item['icon'])).$this->Html->tag('span', $item['text']),
                    '#',
                    array(
                        'escape' => false,
                        'aria-controls'=>$hash,
                        'aria-expanded'=>"true",
                        'class'=>"nav-link collapsed",
                        'data-target'=>"#".$hash,
                        'data-toggle'=>"collapse"
                    )
                ); ?>
                <div aria-labelledby="<?php echo $hash; ?>" class="collapse" data-parent="#accordionSidebar" id="<?php echo $hash; ?>">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">
                            Custom Components:
                        </h6>
                        <?php foreach ($item['items'] as $subItem) {?>
                            <?php echo $this->Html->link(
                                $subItem['text'],
                                $subItem['href'],
                                array('class' => 'collapse-item')
                            ); ?>
                        <?php } ?> 
                    </div>
                </div>
            <?php } ?>
        </li>
    <?php }?>
    <hr class="sidebar-divider d-none d-md-block" />
    <?php echo $this->element('SBAdmin2.sidebar-toggler'); ?>
</ul>
<!-- End of Sidebar -->
