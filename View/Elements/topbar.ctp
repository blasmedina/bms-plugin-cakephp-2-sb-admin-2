<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop">
        <i class="fa fa-bars">
        </i>
    </button>
    <?php // echo $this->element('SBAdmin2.topbar-search'); ?>
    <?php echo $this->element('SBAdmin2.topbar-navbar'); ?>
</nav>
<!-- End of Topbar -->
