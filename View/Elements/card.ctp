<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <!-- <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample"> -->
            <h6 class="m-0 font-weight-bold text-primary"><?php echo $cardTitle; ?></h6>
        <!-- </a> -->
        
        <?php if (isset($cardActions)) { ?>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <?php echo $cardActions; ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="collapse show" id="collapseCardExample" style="">
        <div class="card-body">
            <?php echo $cardbody; ?>
        </div>
    </div>
</div>