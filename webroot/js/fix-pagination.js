$(function() {
    $.each($('ul.pagination a'), function(index, val) {
        var $val = $(val);
        $val.addClass('page-link');
        if (!$val.is('[href]')) {
            $val.attr('href', '#');
            $val.on('click', function(event) {
                event.preventDefault();
            });
        }
    });
});