$(function() {
    var data = null;

    $('[data-model="btn-delete"]').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var button = $(this);
        data = {};
        data.id = button.data('id');
        data.url = button.data('url');
        data.confirm = button.data('confirm');
        data.title = button.data('title');
        data.url = button.data('url');
    });

    $('#modalDelete').find('.delete').on('click', function(event) {
        event.preventDefault();
        eliminar();
    });

    $('#modalDelete').on('shown.bs.modal', function() {
        var modal = $(this);
        modal.find('.modal-title').text(data.title);
        modal.find('.confim').text(data.confirm);
    });

    function eliminar() {
        if (data !== null) {
            var url = data.url + '/' + data.id;
            console.log('eliminar', url);
            data = null;
        }
    }

    // var modal = $('#exampleModal');

    // modal.on('show.bs.modal', function(event) {
    //     var button = $(event.relatedTarget);
    //     var id = button.data('id');
    //     var id = button.data('id');
    //     modal.find('input').val(id);
    // });

    // modal.find('.delete').on('click', function(event) {
    //     event.preventDefault();
    //     var button = $(this);
    //     var id = modal.find('input').val();
    //     var url = button.attr('href') + '/' + id;
    //     console.log(url);
    // });
});