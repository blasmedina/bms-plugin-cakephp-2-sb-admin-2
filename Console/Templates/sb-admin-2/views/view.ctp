<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php echo "<?php \$this->start('summary'); ?>\n"; ?>
	<dl class="row">
<?php
foreach ($fields as $field) {
	$isKey = false;
	if (!empty($associations['belongsTo'])) {
		foreach ($associations['belongsTo'] as $alias => $details) {
			if ($field === $details['foreignKey']) {
				$isKey = true;
?>
		<dt class="col-sm-3"><?php echo "<?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?>"; ?></dt>
		<dd class="col-sm-9">: <?php echo "<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>&nbsp;"; ?></dd>
<?php
				break;
			}
		}
	}
	if ($isKey !== true) {
?>
		<dt class="col-sm-3"><?php echo "<?php echo __('" . Inflector::humanize($field) . "'); ?>"; ?></dt>
		<dd class="col-sm-9">: <?php echo "<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>"; ?></dd>
<?php
	}
}
?>
	</dl>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->start('summaryActions'); ?>\n"; ?>
	<div class="dropdown-header"><?php echo "<?php echo __('Actions'); ?>"; ?>:</div>
	<?php echo "<?php echo \$this->Html->link(__('New {$pluralHumanName}'), array('action' => 'add'), array('class' => 'dropdown-item')); ?>\n"; ?>
	<?php echo "<?php echo \$this->Html->link(__('Edit {$singularHumanName}'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'dropdown-item')); ?>\n"; ?>
	<?php echo "<?php echo \$this->Html->tag('button', __('Delete {$singularHumanName}'), array('class' => 'dropdown-item', 'data-toggle' => 'modal', 'data-target' => '#modalDelete', 'data-id' => \${$singularVar}['{$modelClass}']['{$primaryKey}'], 'data-url' => \$this->Html->url(array('action' => 'delete')), 'data-model' => 'btn-delete', 'data-title' => __('Delete %s', '{$modelClass}'),'data-confirm' => __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}']))); ?>\n"; ?>
	<?php echo "<?php echo \$this->Html->link(__('List {$pluralHumanName}'), array('action' => 'index'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php
	$done = array();
	foreach ($associations as $type => $data) {
		foreach ($data as $alias => $details) {
			if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
?>
	<div class="dropdown-divider"></div>
	<?php echo "<?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => 'dropdown-item')); ?>\n"; ?>
	<?php echo "<?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php
				$done[] = $details['controller'];
			}
		}
	}
?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->append('modal'); ?>\n"; ?>
    <?php echo "<?php echo \$this->element('SBAdmin2.modal-delete'); ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php
if (!empty($associations['hasOne'])) {
	foreach ($associations['hasOne'] as $alias => $details) {
?>
<div class="related">
	<h3><?php echo "<?php echo __('Related " . Inflector::humanize($details['controller']) . "'); ?>"; ?></h3>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])) { ?>\n"; ?>
		<dl>
<?php foreach ($details['fields'] as $field) { ?>
			<dt><?php echo "<?php echo __('" . Inflector::humanize($field) . "'); ?>"; ?></dt>
			<dd><?php echo "<?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n&nbsp;"; ?></dd>
<?php } ?>
		</dl>
	<?php echo "<?php }; ?>\n"; ?>
	<div class="actions">
		<ul>
			<li><?php echo "<?php echo \$this->Html->link(__('Edit " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?></li>\n"; ?>
		</ul>
	</div>
</div>

<?php
	};
};
?>

<?php
if (empty($associations['hasMany'])) {
	$associations['hasMany'] = array();
}
if (empty($associations['hasAndBelongsToMany'])) {
	$associations['hasAndBelongsToMany'] = array();
}

$relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
foreach ($relations as $alias => $details) {
	$otherSingularVar = Inflector::variable($alias);
	$otherPluralHumanName = Inflector::humanize($details['controller']);
?>
<?php echo "<?php \$this->start('related{$alias}'); ?>\n"; ?>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])) { ?>\n"; ?>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead class="thead-dark">
					<tr>
<?php foreach ($details['fields'] as $field) { ?>
						<th><?php echo "<?php echo __('" . Inflector::humanize($field) . "'); ?>"; ?></th>
<?php } ?>
						<th><?php echo "<?php echo __('Actions'); ?>"; ?></th>
					</tr>
				</thead>
				<tbody>
					<?php echo "<?php foreach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}) { ?>\n"; ?>
						<tr>
<?php foreach ($details['fields'] as $field) { ?>
							<td><?php echo "<?php echo \${$otherSingularVar}['{$field}']; ?>"; ?></td>
<?php } ?>
							<td>
								<?php echo "<?php echo \$this->Html->link('<i class=\"fas fa-info\"></i>', array('controller' => '{$details['controller']}', 'action' => 'view', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-info btn-circle btn-sm', 'escape' => false)); ?>\n"; ?>
								<?php echo "<?php echo \$this->Html->link('<i class=\"fas fa-edit\"></i>', array('controller' => '{$details['controller']}', 'action' => 'edit', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-primary btn-circle btn-sm', 'escape' => false)); ?>\n"; ?>
								<?php echo "<?php echo \$this->Html->tag('button', '<i class=\"fas fa-trash\"></i>', array('class' => 'btn btn-danger btn-circle btn-sm', 'data-toggle' => 'modal', 'data-target' => '#modalDelete', 'data-id' => \${$otherSingularVar}['{$details['primaryKey']}'], 'data-url' => \$this->Html->url(array('controller' => '{$details['controller']}', 'action' => 'delete')), 'data-model' => 'btn-delete', 'data-title' => __('Delete %s', '{$alias}'),'data-confirm' => __('Are you sure you want to delete # %s?', \${$otherSingularVar}['{$details['primaryKey']}']))); ?>\n"; ?>
							</td>
						</tr>
					<?php echo "<?php }; ?>\n"; ?>
				</tbody>
			</table>
		</div>
	<?php echo "<?php }; ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->start('related{$alias}Actions'); ?>\n"; ?>
	<div class="dropdown-header"><?php echo "<?php echo __('Actions'); ?>"; ?>:</div>
	<?php echo "<?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->append('related'); ?>\n"; ?>
	<?php echo "<?php echo \$this->element('SBAdmin2.card', array('cardTitle' => __('Related %s', '{$otherPluralHumanName}'), 'cardbody' => \$this->fetch('related{$alias}'), 'cardActions' => \$this->fetch('related{$alias}Actions'))); ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php }; ?>

<?php echo "<?php \$this->layout = 'SBAdmin2.default'; ?>\n"; ?>
<div class="<?php echo $pluralVar; ?> view">
	<h2><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?></h2>
	<?php echo "<?php echo \$this->element('SBAdmin2.card', array('cardTitle' => __('Sumamry'), 'cardbody' => \$this->fetch('summary'), 'cardActions' => \$this->fetch('summaryActions'))); ?>\n"; ?>
	<?php echo "<?php echo \$this->fetch('related'); ?>\n"; ?>
</div>