<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php echo "<?php \$this->start('table'); ?>\n"; ?>
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<thead class="thead-dark">
				<tr>
<?php foreach ($fields as $field) { ?>
					<th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
<?php }; ?>
					<th><?php echo "<?php echo __('Actions'); ?>"; ?></th>
				</tr>
			</thead>
			<tbody>
				<?php echo "<?php foreach (\${$pluralVar} as \${$singularVar}) { ?>\n"; ?>
					<tr>
<?php
			foreach ($fields as $field) {
				$isKey = false;
				if (!empty($associations['belongsTo'])) {
					foreach ($associations['belongsTo'] as $alias => $details) {
						if ($field === $details['foreignKey']) {
							$isKey = true;
?>
						<td><?php echo "<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>"; ?></td>
<?php
							break;
						}
					}
				}
				if ($isKey !== true) {
?>
						<td><?php echo "<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;"; ?></td>
<?php
				}
			}
?>
						<td>
							<?php echo "<?php echo \$this->Html->link('<i class=\"fas fa-info\"></i>', array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-info btn-circle btn-sm', 'escape' => false)); ?>\n";?>
							<?php echo "<?php echo \$this->Html->link('<i class=\"fas fa-edit\"></i>', array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-primary btn-circle btn-sm', 'escape' => false)); ?>\n"; ?>
							<?php echo "<?php echo \$this->Html->tag('button', '<i class=\"fas fa-trash\"></i>', array('class' => 'btn btn-danger btn-circle btn-sm', 'data-toggle' => 'modal', 'data-target' => '#exampleModal', 'data-id' => \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n"; ?>
						</td>
					</tr>
				<?php echo "<?php } ?>\n"; ?>
			</tbody>
		</table>
	</div>
	<p>
		<?php echo "<?php echo \$this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>\n"; ?>
	</p>
	<ul class="pagination">
		<?php echo "<?php echo \$this->Paginator->prev(__('Previous'), array('tag' => 'li', 'class' => 'page-item'), null, array('class' => 'disabled page-item', 'tag' => 'li', 'disabledTag' => 'a')); ?>\n"; ?>
		<?php echo "<?php echo \$this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'class' => 'page-item', 'currentClass' => 'active')); ?>\n"; ?>
		<?php echo "<?php echo \$this->Paginator->next(__('Next'), array('tag' => 'li', 'class' => 'page-item'), null, array('class' => 'disabled page-item', 'tag' => 'li', 'disabledTag' => 'a')); ?>\n"; ?>
	</ul>
	<?php echo "<?php \$this->Html->script('SBAdmin2.js/fix-pagination', array('inline' => false)); ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->start('tableActions'); ?>\n"; ?>
	<div class="dropdown-header"><?php echo "<?php echo __('Actions'); ?>"; ?>:</div>
	<?php echo "<?php echo \$this->Html->link(__('New %s', '{$singularHumanName}'), array('action' => 'add'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php $done = array();
	foreach ($associations as $type => $data) {
		foreach ($data as $alias => $details) {
			if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
?>
	<?php echo "<?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => 'dropdown-item')); ?>\n"; ?>
	<?php echo "<?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php
				$done[] = $details['controller'];
			}
		}
	}
?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->start('modal'); ?>\n"; ?>
    <?php echo "<?php echo \$this->element('SBAdmin2.modal-delete', array('modelClass' => '{$singularHumanName}')); ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->layout = 'SBAdmin2.default'; ?>\n"; ?>
<div class="<?php echo $pluralVar; ?> index">
	<h2><?php echo "<?php echo __('{$singularHumanName}'); ?>"; ?></h2>
	<?php echo "<?php echo \$this->element('SBAdmin2.card', array('cardTitle' => __('Records'), 'cardbody' => \$this->fetch('table'), 'cardActions' => \$this->fetch('tableActions'))); ?>\n"; ?>
</div>
