<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php echo "<?php \$this->start('form'); ?>\n"; ?>
	<div class="<?php echo $pluralVar; ?> form">
		<?php echo "<?php echo \$this->Form->create('{$modelClass}', array('inputDefaults' => array('div' => 'form-group', 'class' => 'form-control'))); ?>\n"; ?>
			<fieldset>
				<legend><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></legend>
<?php
		foreach ($fields as $field) {
			if (strpos($action, 'add') !== false && $field === $primaryKey) {
				continue;
			} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
?>
				<?php echo "<?php echo \$this->Form->input('{$field}'); ?>\n"; ?>
<?php
			}
		}
		if (!empty($associations['hasAndBelongsToMany'])) {
			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
?>
				<?php echo "<?php echo \$this->Form->input('{$assocName}'); ?>\n"; ?>
<?php
			}
		}
?>
			</fieldset>
			<?php echo "<?php echo \$this->Form->submit(__('Submit'), array('div' => false, 'class' => 'btn btn-primary')); ?>\n"; ?>
		<?php echo "<?php echo \$this->Form->end(); ?>\n"; ?>
	</div>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->start('formActions'); ?>\n"; ?>
	<div class="dropdown-header"><?php echo "<?php echo __('Actions'); ?>"; ?>:</div>
<?php if (strpos($action, 'add') === false) { ?>
	<?php echo "<?php echo \$this->Html->tag('button', __('Delete'), array('class' => 'dropdown-item', 'data-toggle' => 'modal', 'data-target' => '#modalDelete', 'data-id' => \$this->Form->value('{$modelClass}.{$primaryKey}'), 'data-url' => \$this->Html->url(array('action' => 'delete')), 'data-model' => 'btn-delete', 'data-title' => __('Delete %s', '{$modelClass}'), 'data-confirm' => __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}')))); ?>\n"; ?>
<?php }; ?>
	<?php echo "<?php echo \$this->Html->link(__('List " . $pluralHumanName . "'), array('action' => 'index'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php
$done = array();
foreach ($associations as $type => $data) {
	foreach ($data as $alias => $details) {
		if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
?>
	<div class="dropdown-divider"></div>
	<?php echo "<?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'), array('class' => 'dropdown-item')); ?>\n"; ?>
	<?php echo "<?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'), array('class' => 'dropdown-item')); ?>\n"; ?>
<?php
			$done[] = $details['controller'];
		}
	}
}
?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->append('modal'); ?>\n"; ?>
    <?php echo "<?php echo \$this->element('SBAdmin2.modal-delete'); ?>\n"; ?>
<?php echo "<?php \$this->end(); ?>\n"; ?>

<?php echo "<?php \$this->layout = 'SBAdmin2.default'; ?>\n"; ?>
<?php echo "<?php echo \$this->element('SBAdmin2.card', array('cardTitle' => __('" . Inflector::humanize($action) . "'), 'cardbody' => \$this->fetch('form'), 'cardActions' => \$this->fetch('formActions'))); ?>\n"; ?>
